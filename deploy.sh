#!/usr/bin/env bash
mkdir .generated
for f in templates/*.yml
do
 envsubst < $f > .generated/${f##*/}
 kubectl apply -f .generated/${f##*/}
done
kubectl rollout status deployment.v1.apps/$POD
kubectl set image $POD=$IMAGE_TAG -f .generated/pod.yml --record
