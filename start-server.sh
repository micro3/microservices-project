#!/bin/bash 
rm -rf target
mvn clean package -DskipTests
docker exec -ti sunny_project bash -c 'java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 /microservices_project/target/sunny_cp_project-0.0.1.jar'
