$(document).ready(() => {
    setupLogout()
});


function setupLogout() {
    $("#logout").click(() => {
        $.ajax({
            url: "/user/logout",
            type: "POST",
            data: {
                "userName":localStorage.getItem("userName"),
            },
            success: function () {
                localStorage.clear();
                window.location = "/index.html";
            },
            error: function (xhr) {
                alert(xhr.responseText)
            }
        });
        return false;
    });

};