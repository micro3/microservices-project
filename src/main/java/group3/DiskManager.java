package group3;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DiskManager {

	public void saveToDisk(List<MultipartFile> files, String path) throws IOException {
		createDirIfnNotExist(path);

		for (MultipartFile file : files) {
			if (file == null) {
				continue;
			}
			saveToDisk(file, path);
		}
	}

	public void createDirIfnNotExist(String path) {
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdir();
		}
	}

	private void saveToDisk(MultipartFile file, String path) throws IOException {
		String filePath = path + file.getOriginalFilename();
		File dest = new File(filePath);
		file.transferTo(dest);
	}
}
