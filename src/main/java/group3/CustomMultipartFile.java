package group3;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;

public class CustomMultipartFile implements MultipartFile
{
	private final byte[] fileContent;
	private final String filename;

	public CustomMultipartFile(final byte[] fileContent, String filename)
	{
		this.fileContent = fileContent.clone();
		this.filename = filename;
	}

	@Override
	public String getName()
	{
		return this.filename;
	}

	@Override
	public String getOriginalFilename()
	{
		return this.filename;
	}

	@Override
	public String getContentType()
	{
		return null;
	}

	@Override
	public boolean isEmpty()
	{
		return fileContent == null || fileContent.length == 0;
	}

	@Override
	public long getSize()
	{
		return fileContent.length;
	}

	@Override
	public byte[] getBytes()
	{
		return fileContent;
	}

	@Override
	public InputStream getInputStream()
	{
		return new ByteArrayInputStream(fileContent);
	}

	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException
	{
		Files.newOutputStream(dest.toPath()).write(fileContent);
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public String getFilename() {
		return filename;
	}

}
