package group3.repository;

import group3.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	Optional<User> findUserByUsernameAndPassword(String name, String password);

	Optional<User> findFirstByUsername(String name);

	Optional<User> findUserByUsernameAndSid(String name, String token);
}
