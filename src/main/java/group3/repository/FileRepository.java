package group3.repository;

import group3.entity.File;
import group3.sunny.FileType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileRepository extends CrudRepository<File, Integer> {

	Optional<File> findFirstByUsernameAndFilename(String username, String filename);

	Optional<File> findFirstByUsernameAndFilenameAndFiletype(String username, String filename, FileType fileType);

	List<File> findAllByUsername(String username);
}

