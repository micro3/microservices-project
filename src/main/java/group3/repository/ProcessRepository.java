package group3.repository;

import group3.entity.Process;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProcessRepository extends CrudRepository<Process, Integer> {
	Optional<Process> findByPid(String pid);
}
