package group3.entity;

import group3.sunny.FileType;

import javax.persistence.*;

@Entity
@Table(name = "files")
public class File {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	private String username;

	private byte[] file;

	private String filename;

	private FileType filetype;

	public File() {
		//Entities need to have empty constructor
	}

	public File(String username, String filename, byte[] file, FileType type) {
		this.username = username;
		this.file = file.clone();
		this.filename = filename;
		this.filetype = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file.clone();
	}

	public FileType getFiletype() {
		return filetype;
	}

	public void setFiletype(FileType filetype) {
		this.filetype = filetype;
	}

}
