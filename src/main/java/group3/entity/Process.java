package group3.entity;

import group3.sunny.State;

import javax.persistence.*;

@Entity
@Table(name = "processes")
public class Process {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	private String username;

	private String pid;

	private State state;

	private String result;

	public Process() {
		//Entities need to have empty constructor
	}

	public Process(String username, String pid, State state) {
		this.username = username;
		this.pid = pid;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}