package group3.interceptor;

import group3.entity.User;
import group3.repository.UserRepository;
import group3.sunny.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@CrossOrigin
@Component
public class AdminInterceptor implements HandlerInterceptor {
	@Autowired
	UserRepository userRepository;

	@CrossOrigin
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String userName;
		try {
			userName = request.getParameterMap().get("userName")[0];
		} catch (NullPointerException e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return false;
		}
		Optional<User> userOpt = userRepository.findFirstByUsername(userName);
		if (userOpt.isPresent() && userOpt.get().getRole().equals(Role.ADMIN)) {
			return true;
		}
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		return false;
	}
}