package group3.interceptor;


import group3.entity.User;
import group3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;

@Component
public class LoginInterceptor implements HandlerInterceptor {
	@Autowired
	UserRepository userRepository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Map<String, String[]> params = request.getParameterMap();
		String userName;
		String sid;
		try{
			userName = params.get("userName")[0];
			sid = request.getHeader("authorization");
		}catch (NullPointerException e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return false;
		}
		Optional<User> userOpt = userRepository.findUserByUsernameAndSid(userName, sid);
		if (userOpt.isPresent()) {
			return true;
		}
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		return false;
	}
}