package group3.interceptor;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Component
public class InterceptorConfig extends WebMvcConfigurerAdapter {

	@Autowired
	LoginInterceptor loginInterceptor;

	@Autowired
	AdminInterceptor adminInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		List<String> paths = Lists.newArrayList();
		paths.add("/user");
		paths.add("/user/login");
		paths.add("/user/logout");
		paths.add("/*.html");
		paths.add("/images/*");
		paths.add("/js/*");
		registry.addInterceptor(loginInterceptor).excludePathPatterns(paths);

		List<String> adminPaths = Lists.newArrayList();
		adminPaths.add("/admin/files");
		adminPaths.add("/admin/users");
		registry.addInterceptor(adminInterceptor).addPathPatterns(adminPaths);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/").setViewName("forward:/index.html");
	}
}
