package group3.rest.beans;

import group3.entity.File;
import group3.entity.User;
import group3.repository.FileRepository;
import group3.repository.UserRepository;
import group3.rest.interfaces.AdminFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class AdminController implements AdminFacade {

	@Autowired
	FileRepository fileRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public ResponseEntity deleteUsersFiles(@RequestHeader(value = "authorization") String authorization, String userName, String filesUserName) {
		if (!isAuthorized(userName, authorization)){
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		List<File> files = fileRepository.findAllByUsername(filesUserName);
		if (files.isEmpty()) {
			return new ResponseEntity<>(filesUserName + " does not have any files.", HttpStatus.NOT_FOUND);
		}
		files.forEach(fileRepository::delete);
		return new ResponseEntity<>("Files of user " + filesUserName + " deleted successfully.", HttpStatus.OK);
	}

	@Override
	public ResponseEntity deleteUser(@RequestHeader(value = "authorization") String authorization, String userName, String userNameToDelete) {
		if (!isAuthorized(userName, authorization)){
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		Optional<User> userOptional = userRepository.findFirstByUsername(userNameToDelete);
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(userNameToDelete + " does not exist.", HttpStatus.NOT_FOUND);
		}
		userRepository.delete(userOptional.get());
		return new ResponseEntity<>(userNameToDelete + " deleted successfully.", HttpStatus.OK);
	}

	private boolean isAuthorized(String userName, String authorization) {
		Optional<User> userOpt = userRepository.findUserByUsernameAndSid(userName, authorization);
		return userOpt.isPresent();
	}
}
