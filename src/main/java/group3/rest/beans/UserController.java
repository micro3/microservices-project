package group3.rest.beans;

import group3.PasswordAuthentication;
import group3.entity.User;
import group3.repository.UserRepository;
import group3.rest.interfaces.UserFacade;
import group3.sunny.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "/user")
public class UserController implements UserFacade {

	@Autowired
	UserRepository userRepository;

	private PasswordAuthentication passwordAuthentication = new PasswordAuthentication();

	@Override
	public ResponseEntity createUser(String userName, String password, String role) {
		Optional<User> optionalUser = userRepository.findFirstByUsername(userName);
		if (optionalUser.isPresent()) {
			return new ResponseEntity<>("User name already taken", HttpStatus.CONFLICT);
		}
		String hashedPassword = passwordAuthentication.hash(password.toCharArray());

		User user = new User(userName, hashedPassword, Role.valueOf(role));
		userRepository.save(user);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity loginUser(String userName, String password) {
		Optional<User> optionalUser = userRepository.findFirstByUsername(userName);

		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>("Given user does not exist", HttpStatus.NOT_FOUND);
		}
		boolean isAuthenticated = passwordAuthentication.authenticate(password.toCharArray(), optionalUser.get().getPassword());
		if (!isAuthenticated) {
			return new ResponseEntity<>("Wrong credentials", HttpStatus.UNAUTHORIZED);
		}

		User user = optionalUser.get();
		String uuid = UUID.randomUUID().toString();
		user.setSid(uuid);
		userRepository.save(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@Override
	public ResponseEntity logoutUser(String userName) {
		Optional<User> optionalUser = userRepository.findFirstByUsername(userName);
		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>("You are not logged in", HttpStatus.NOT_FOUND);
		}
		User user = optionalUser.get();
		user.setSid(null);
		userRepository.save(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
