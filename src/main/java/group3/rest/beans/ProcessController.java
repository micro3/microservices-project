package group3.rest.beans;

import group3.CustomMultipartFile;
import group3.DiskManager;
import group3.entity.File;
import group3.entity.Process;
import group3.repository.FileRepository;
import group3.repository.ProcessRepository;
import group3.rest.interfaces.ProcessFacade;
import group3.sunny.FileType;
import group3.sunny.Options;
import group3.sunny.State;
import group3.sunny.SunnyExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
public class ProcessController implements ProcessFacade {
	private final static transient String UPLOADS_DIR = "/files/";

	@Autowired
	FileRepository fileRepository;

	@Autowired
	ProcessRepository processRepository;

	DiskManager diskManager = new DiskManager();

	@Override
	public ResponseEntity deleteFile(String userName,
									 String fileName) {
		Optional<File> fileOpt = fileRepository.findFirstByUsernameAndFilename(userName, fileName);
		if (fileOpt.isPresent()) {
			fileRepository.delete(fileOpt.get());
			return new ResponseEntity<>("File deleted successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Cannot delete a File. File not found", HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity uploadKbFiles(String userName,
										Optional<MultipartFile> cspFile,
										Optional<MultipartFile> copFile) {
		List<MultipartFile> files = new ArrayList<>();
		copFile.ifPresent(files::add);
		cspFile.ifPresent(files::add);

		try {
			String path = UPLOADS_DIR + userName + "/kb/";
			diskManager.createDirIfnNotExist(path);
			diskManager.saveToDisk(files, path);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<>("Cannot save files to disk.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Files have been save on a disk.", HttpStatus.OK);
	}

	@Override
	public ResponseEntity uploadFiles(String userName,
									  Optional<MultipartFile> model,
									  Optional<MultipartFile> dznFile,
									  Optional<MultipartFile> mznFile) {
		if (!mznFile.isPresent()) {
			return new ResponseEntity<>("Please send mzn File.", HttpStatus.BAD_REQUEST);
		}
		if (userName == null) {
			return new ResponseEntity<>("Please introduce yourself.", HttpStatus.BAD_REQUEST);
		}
		model.ifPresent(m -> save(m, userName, FileType.MODEL));
		dznFile.ifPresent(d -> save(d, userName, FileType.DZN));
		save(mznFile.get(), userName, FileType.MZN);

		return new ResponseEntity<>("Files have been save on a disk.", HttpStatus.OK);
	}

	private void save(MultipartFile file, String username, FileType type) {
		Optional<File> fileOptional = fileRepository.findFirstByUsernameAndFilename(username, file.getOriginalFilename());
		if (fileOptional.isPresent()) {
			fileRepository.save(fileOptional.get());
		} else {
			try {
				fileRepository.save(new File(username, file.getOriginalFilename(), file.getBytes(), type));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public ResponseEntity triggerSunny(String userName,
									   String mznFileName,
									   Optional<String> dznFileName,
									   @RequestParam("-P") Optional<String> optionCapitalP,
									   @RequestParam("-K") Optional<String> optionCapitalK,
									   @RequestParam("-T") Optional<String> optionCapitalT,
									   @RequestParam("-A") Optional<String> optionCapitalA,
									   @RequestParam("-R") Optional<String> optionCapitalR,
									   @RequestParam("-b") Optional<String> optionB,
									   @RequestParam("-p") Optional<String> optionP,
									   @RequestParam("-s") Optional<String> optionS,
									   Optional<String> modelFileName) {
		HashMap<Options, String> optionsMap = new HashMap<>();
		optionCapitalP.ifPresent(s -> optionsMap.put(Options.P, s));
		optionCapitalT.ifPresent(s -> optionsMap.put(Options.T, s));
		optionCapitalR.ifPresent(s -> optionsMap.put(Options.R, s));
		optionCapitalA.ifPresent(s -> optionsMap.put(Options.A, s));
		optionB.ifPresent(s -> optionsMap.put(Options.b, s));
		optionS.ifPresent(s -> optionsMap.put(Options.s, s));
		optionP.ifPresent(s -> optionsMap.put(Options.p, s));
		optionCapitalK.ifPresent(s -> optionsMap.put(Options.K, UPLOADS_DIR + userName + "/kb/"));
		return triggerSunnyExecution(userName, mznFileName, modelFileName, dznFileName, optionsMap);
	}

	@Override
	public ResponseEntity getFeatures(String userName,
									  String mznFileName,
									  Optional<String> modelFileName,
									  Optional<String> dznFileName) {
		Optional<File> mznOpt = fileRepository.findFirstByUsernameAndFilename(userName, mznFileName);
		if (!mznOpt.isPresent()) {
			return new ResponseEntity<>("Files " + mznFileName + " has not been yet uploaded", HttpStatus.NOT_FOUND);
		}

		saveFilesToDisk(userName, mznFileName, modelFileName.orElse(""), dznFileName.orElse(""));
		SunnyExecutor sunnyExecutor = new SunnyExecutor();
		try {
			String result = sunnyExecutor.extractFeatures(mznFileName, dznFileName, UPLOADS_DIR + userName + "/");
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<>("Cannot extract features. Has " + mznFileName + " been uploaded?", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity getResult(String userName, String sunnyId) {
		return processRepository
				.findByPid(sunnyId)
				.<ResponseEntity>map(process -> new ResponseEntity<>(process, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@Override
	public ResponseEntity deleteResult(String userName, String sunnyId) {
		Optional<Process> process = processRepository.findByPid(sunnyId);
		process.ifPresent(processRepository::delete);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private ResponseEntity triggerSunnyExecution(String userName,
												 String originalFilename,
												 Optional<String> modelFileNameOpt,
												 Optional<String> dznFileNameOpt,
												 Map<Options, String> options) {
		String dznFileName = dznFileNameOpt.orElse(null);
		String modelFileName = modelFileNameOpt.orElse(null);
		if (!saveFilesToDisk(userName, originalFilename, modelFileName, dznFileName)) {
			return new ResponseEntity<>("Cannot save files to disk.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		String uuid = UUID.randomUUID().toString();

		Thread t = new Thread(() -> {
			Process process = new Process(userName, uuid, State.IN_PROGRESS);
			processRepository.save(process);
			String sunnyResult = "";
			try {
				String path = UPLOADS_DIR + userName + "/";
				SunnyExecutor sunnyExecutor = new SunnyExecutor(options);
				sunnyResult = sunnyExecutor.executeSunnyCPAndGetResult(originalFilename, path, dznFileNameOpt);
				process.setState(State.SUCCESS);
			} catch (IOException e) {
				sunnyResult = "Error with sunny-cp execution.";
				process.setState(State.ERROR);
			} finally {
				process.setResult(sunnyResult);
				processRepository.save(process);
			}
		});
		t.start();

		MultiValueMap<String, String> headers = new HttpHeaders();
		headers.add("Sunny-Id", uuid);
		return new ResponseEntity<>("Server triggered the execution of sunny-cp process. Id in the headers", headers, HttpStatus.ACCEPTED);
	}

	private boolean saveFilesToDisk(String userName, String mznFileName, String modelFileName, String dznFileName) {
		List<MultipartFile> files = new ArrayList<>();
		Optional<File> mznOpt = fileRepository.findFirstByUsernameAndFilenameAndFiletype(userName, mznFileName, FileType.MZN);
		Optional<File> dznOpt = fileRepository.findFirstByUsernameAndFilenameAndFiletype(userName, dznFileName, FileType.DZN);
		Optional<File> modelOpt = fileRepository.findFirstByUsernameAndFilenameAndFiletype(userName, modelFileName, FileType.MODEL);

		mznOpt.ifPresent(m -> files.add(new CustomMultipartFile(m.getFile(), m.getFilename())));
		modelOpt.ifPresent(m -> files.add(new CustomMultipartFile(m.getFile(), m.getFilename())));
		dznOpt.ifPresent(d -> files.add(new CustomMultipartFile(d.getFile(), d.getFilename())));

		try {
			String path = UPLOADS_DIR + userName + "/";
			diskManager.createDirIfnNotExist(path);
			diskManager.saveToDisk(files, path);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
