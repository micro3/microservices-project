package group3.rest.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface UserFacade {

	@PostMapping
	ResponseEntity createUser(@RequestParam("userName") String userName,
							  @RequestParam("password") String password,
							  @RequestParam("role") String role);

/*	@GetMapping
	ResponseEntity userInfo(@PathParam("userName") String userName,
							@RequestHeader HttpHeaders headers);*/

	@PostMapping("/login")
	ResponseEntity loginUser(@RequestParam("userName") String userName,
							 @RequestParam("password") String password);

	@PostMapping("/logout")
	ResponseEntity logoutUser(@RequestParam("userName") String userName);

}
