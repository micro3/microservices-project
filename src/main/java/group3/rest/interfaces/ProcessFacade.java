package group3.rest.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.util.Optional;

public interface ProcessFacade {
	String USER_NAME = "userName";
	String SUNNY_ID = "sunnyId";
	String MZN_FILE_NAME = "mznFileName";
	String DZN_FILE_NAME = "dznFileName";
	String MODEL_FILE_NAME = "modelFileName";

	@DeleteMapping("/files")
	ResponseEntity deleteFile(@PathParam(USER_NAME) String userName,
							  @PathParam("fileName") String fileName);

	@PostMapping("/kb")
	ResponseEntity uploadKbFiles(@PathParam(USER_NAME) String userName,
								 @RequestParam("csp") Optional<MultipartFile> cspFile,
								 @RequestParam("cop") Optional<MultipartFile> copFile);

	@PostMapping("/files")
	ResponseEntity uploadFiles(@PathParam(USER_NAME) String userName,
							   @RequestParam("model") Optional<MultipartFile> modelFile,
							   @RequestParam("dznFile") Optional<MultipartFile> dznFile,
							   @RequestParam("mznFile") Optional<MultipartFile> mznFile);

	@GetMapping("/trigger")
	ResponseEntity triggerSunny(@PathParam(USER_NAME) String userName,
								@PathParam(MZN_FILE_NAME) String mznFileName,
								@RequestParam(DZN_FILE_NAME) Optional<String> dznFileName,
								@RequestParam("-P") Optional<String> optionCapitalP,
								@RequestParam("-K") Optional<String> optionCapitalK,
								@RequestParam("-T") Optional<String> optionCapitalT,
								@RequestParam("-A") Optional<String> optionCapitalA,
								@RequestParam("-R") Optional<String> optionCapitalR,
								@RequestParam("-b") Optional<String> optionB,
								@RequestParam("-p") Optional<String> optionP,
								@RequestParam("-s") Optional<String> optionS,
								@RequestParam(MODEL_FILE_NAME) Optional<String> modelFileName);

	@GetMapping("/features")
	ResponseEntity getFeatures(@PathParam(USER_NAME) String userName,
							   @PathParam(MZN_FILE_NAME) String mznFileName,
							   @RequestParam(MODEL_FILE_NAME) Optional<String> modelFileName,
							   @RequestParam(DZN_FILE_NAME) Optional<String> dznFileName);

	@GetMapping("/result")
	ResponseEntity getResult(@PathParam(USER_NAME) String userName,
							 @PathParam(SUNNY_ID) String sunnyId);

	@DeleteMapping("/result")
	ResponseEntity deleteResult(@PathParam(USER_NAME) String userName,
								@PathParam(SUNNY_ID) String sunnyId);
}
