package group3.rest.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;

public interface AdminFacade {
	String AUTHORIZATION = "authorization";

	@DeleteMapping("/admin/files")
	ResponseEntity deleteUsersFiles(@RequestHeader(value=AUTHORIZATION) String sid,
									@PathParam("userName") String userName,
									@RequestParam("filesUserName") String filesUserName);

	@DeleteMapping("/admin/users")
	ResponseEntity deleteUser(@RequestHeader(value=AUTHORIZATION) String sid,
							  @PathParam("userName") String userName,
							  @RequestParam("userNameToDelete") String userNameToDelete);
}
