package group3.sunny;

public enum Role {
	USER("user"),
	ADMIN("admin");

	private final String role;

	Role(String s) {
		this.role = s;
	}

	public String getType() {
		return role;
	}
}
