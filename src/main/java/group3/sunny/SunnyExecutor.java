package group3.sunny;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

public class SunnyExecutor {

	 Map<Options, String> options;
	private static transient final String SUNNY_CP = "sunny-cp";
	private static transient final String MZN2FEAT = "mzn2feat";

	public SunnyExecutor() {
	}

	public SunnyExecutor(Map<Options, String> options) {
		this.options = options;
	}

	public String executeSunnyCPAndGetResult(String fileName, String directory, Optional<String> dznFileName) throws IOException {
		ProcessBuilder processBuilder = new ProcessBuilder();
		String[] s = this.prepareCommands(fileName, dznFileName);
		processBuilder.command(s);
		processBuilder.directory(new File(directory));

		StringBuilder stringBuilder = execProcess(processBuilder);
		return stringBuilder.toString();
	}

	public String extractFeatures(String mznFileName, Optional<String> dznFileName, String directory) throws IOException {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command(MZN2FEAT, "-o", "pp", "-i", mznFileName);
		dznFileName.ifPresent(s1 -> processBuilder.command(MZN2FEAT, "-o", "pp", "-d", s1, "-i", mznFileName));
		processBuilder.directory(new File(directory));

		StringBuilder stringBuilder = execProcess(processBuilder);
		String result = stringBuilder.toString();
		if (result.isEmpty()) {
			throw new IOException();
		}
		return result;
	}

	private StringBuilder execProcess(ProcessBuilder processBuilder) throws IOException {
		Process process = processBuilder.start();
		BufferedReader reader =
				new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(System.getProperty("line.separator"));
		}
		return stringBuilder;
	}

	private String[] prepareCommands(String filename, Optional<String> dznFileName) {
		ArrayList<String> commands = new ArrayList<>();
		commands.add(SUNNY_CP);
		this.options.forEach((key, value) -> {
			commands.add(key.getOption());
			commands.add(value);
		});
		commands.add(filename);
		dznFileName.ifPresent(commands::add);
		return commands.toArray(new String[0]);
	}
}
