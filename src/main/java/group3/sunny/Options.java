package group3.sunny;

public enum Options {
	P("-P"),
	K("-K"),
	R("-R"),
	A("-A"),
	b("-b"),
	s("-s"),
	p("-p"),
	T("-T");

	private final String option;

	Options(String s) {
		this.option = s;
	}

	public String getOption() {
		return option;
	}
}
