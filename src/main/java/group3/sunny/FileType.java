package group3.sunny;

public enum FileType {
	MZN(0),
	MODEL(1),
	DZN(2);

	private final int type;

	FileType(int s) {
		this.type = s;
	}

	public int getType() {
		return type;
	}
}
