package group3.sunny;

public enum State {

	SUCCESS(0),
	IN_PROGRESS(1),
	ERROR(2);

	private final int state;

	State(int s) {
		this.state = s;
	}

	public int getState() {
		return state;
	}
}
