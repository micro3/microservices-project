package group3.rest.beans;

import group3.App;
import group3.entity.Process;
import group3.entity.User;
import group3.repository.FileRepository;
import group3.repository.ProcessRepository;
import group3.repository.UserRepository;
import group3.sunny.State;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class ProcessControllerTest {

	@Autowired
	ServletWebServerApplicationContext server;

	@MockBean
	UserRepository userRepository;

	@MockBean
	FileRepository fileRepository;

	@MockBean
	ProcessRepository processRepository;

	@LocalServerPort
	int port;
	private static final String USER_1 = "MarlonBrando";
	private static final String USER_2 = "AlPacino";
	private static final String USER_NON_EXISITNG = "BrigitteBardot";
	private static final String MZN_FILENAME_NON_EXISITNG = "IDontExist.mzn";
	private static final String TEST_SID = "random_sid";
	private static final String TEST_PID = "random_pid";
	private static final String NON_EXISTING_PID = "non_existing_pid";
	private static final String MZN_FILENAME = "tenpenki_1.mzn";
	private static final String MODEL_FILENAME = "tenpenki.model.mzn";
	private static final group3.entity.File FILE = new group3.entity.File();
	private static final Process PROCESS = new Process(USER_1, TEST_PID, State.SUCCESS);
	private final String UPLOADS_DIR = "/files/";
	private final String model = "model";
	private final String mznFile = "mznFile";
	private final String userName = "userName";
	private final String authorization = "authorization";

	private String BASE_ADDRESS = "";
	private String FILES_ENDPOINT = "";
	private String TRIGGER_ENDPOINT = "";
	private String RESULT_ENDPOINT = "";
	private String FEATURES_ENDPOINT = "";

	private static FileSystemResource mznFSR;
	private static FileSystemResource modelFSR;

	@BeforeClass
	public static void generalSetup() throws Exception {
		File mzn = ResourceUtils.getFile("classpath:mznfiles/tenpenki_1.mzn");
		File model = ResourceUtils.getFile("classpath:mznfiles/tenpenki.model.mzn");
		mznFSR = new FileSystemResource(mzn);
		modelFSR = new FileSystemResource(model);

	}

	@Before
	public void setup() {
		BASE_ADDRESS = "http://localhost:" + port + "/";
		FILES_ENDPOINT = BASE_ADDRESS + "files/";
		TRIGGER_ENDPOINT = BASE_ADDRESS + "trigger/";
		RESULT_ENDPOINT = BASE_ADDRESS + "result/";
		FEATURES_ENDPOINT = BASE_ADDRESS + "features/";

		Mockito.when(userRepository.findUserByUsernameAndSid(USER_1, TEST_SID))
				.thenReturn(Optional.of(new User()));
		Mockito.when(userRepository.findUserByUsernameAndSid(USER_2, TEST_SID))
				.thenReturn(Optional.of(new User()));
		Mockito.when(fileRepository.findFirstByUsernameAndFilename(USER_1, MZN_FILENAME))
				.thenReturn(Optional.of(FILE));
		Mockito.when(fileRepository.save(FILE))
				.thenReturn(FILE);
		Mockito.when(processRepository.findByPid(TEST_PID))
						.thenReturn(Optional.of(PROCESS));
		Mockito.when(processRepository.findByPid(NON_EXISTING_PID))
						.thenReturn(Optional.empty());

	}

	@Test
	public void testUploadingFiles() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(mznFile, mznFSR);
		parts.add(model, modelFSR);
		parts.add(userName, USER_1);
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(FILES_ENDPOINT, HttpMethod.POST, entity, String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}

	@Test
	public void testUploadingFilesWithOnlyMzn() {
		registerUser(USER_1, mznFSR);
	}

	@Test
	public void testUploadingFilesWithoutMznButWithModel() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(model, modelFSR);
		parts.add(userName, USER_1);
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(FILES_ENDPOINT, HttpMethod.POST, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
			assertThat(e.getResponseBodyAsString(), equalTo("Please send mzn File."));
		}
	}
	@Test
	public void testUploadingFilesWithoutUserName() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(model, modelFSR);
		parts.add(mznFile, mznFSR);
		HttpEntity<String> entity = createAuthorizationHeader(parts);

		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(FILES_ENDPOINT, HttpMethod.POST, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
		}
	}

	@Test
	public void testDeletingNotUploadedFiles() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(FILES_ENDPOINT + "?fileName=" + modelFSR.getFilename() + "&userName=" + USER_1, HttpMethod.DELETE, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
			assertThat(e.getResponseBodyAsString(), equalTo("Cannot delete a File. File not found"));
		}
	}
	@Test
	public void testDeletingAnothersUserFiles() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(FILES_ENDPOINT + "?fileName=" + mznFSR.getFilename() + "&userName=" + USER_1, HttpMethod.DELETE, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
			assertThat(e.getResponseBodyAsString(), equalTo("Cannot delete a File. File not found"));
		}
	}
	@Test
	public void testDeletingFiles() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(FILES_ENDPOINT + "?fileName=" + mznFSR.getFilename() + "&userName=" + USER_1, HttpMethod.DELETE, entity, String.class);

		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(response.getBody(), equalTo("File deleted successfully."));
	}

	@Test
	public void testTriggeringSunny() {
		registerUser(USER_1, mznFSR, modelFSR);
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);

		RestTemplate restTemplate = new RestTemplate();
		String pathWithParams = TRIGGER_ENDPOINT + "?MZN_FILE_NAME=" + mznFSR.getFilename() + "&userName=" + USER_1;
		ResponseEntity<String> response = restTemplate.exchange(pathWithParams, HttpMethod.GET, entity, String.class);

		triggerAssertions(response);
	}

	@Test
	public void testTriggeringNotExistingSunny() {
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);

		String pathWithParams = TRIGGER_ENDPOINT + "?fileName=" + MZN_FILENAME_NON_EXISITNG + "&userName=" + USER_1;
		ResponseEntity<String> response = restTemplate.exchange(pathWithParams, HttpMethod.GET, entity, String.class);

		triggerAssertions(response);
	}
	@Test
	public void testTriggeringNotExistingUser() {
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		String pathWithParams = TRIGGER_ENDPOINT + "?fileName=" + mznFSR.getFilename() + "&userName=" + USER_NON_EXISITNG;
		try {
			restTemplate.exchange(pathWithParams, HttpMethod.GET, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
		}
	}

	/*@Test
	public void testGetResult() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);

		RestTemplate restTemplate = new RestTemplate();
		String pathWithParam = RESULT_ENDPOINT + "?userName=" + USER_1+"sunnyId=" + TEST_PID;
		ResponseEntity<String> response = restTemplate.exchange(pathWithParam, HttpMethod.GET, entity, String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
	@Test
	public void testGetResultFromNonExistingSunnyExecution() {
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		String pathWithParam = RESULT_ENDPOINT + "?userName=" + USER_1+ "?sunnyId=NON_EXISTING_PID";
		try {
			restTemplate.exchange(pathWithParam, HttpMethod.GET, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
			assertThat(e.getResponseBodyAsString(), equalTo("Error with sunny-cp execution."));
		}
	}*/
/*
	@Test
	public void testGetFeaturesWithNonExistingUser() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		String pathWithParam = FEATURES_ENDPOINT + "?userName=" + USER_NON_EXISITNG + "&mznFileName=" + mznFSR.getFilename();
		try {
			ResponseEntity<String> response = restTemplate.exchange(pathWithParam, HttpMethod.GET, entity, String.class);
		} catch (HttpServerErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
			assertThat(e.getResponseBodyAsString(), equalTo("Cannot extract features. Has " + mznFSR.getFilename() + " been uploaded?"));
		}
	}

	@Test
	public void testGetFeaturesOfNonExistingFile() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		String pathWithParam = FEATURES_ENDPOINT + "?userName=" + USER_1 + "&mznFileName=" + MZN_FILENAME_NON_EXISITNG;
		try {
			restTemplate.exchange(pathWithParam, HttpMethod.GET, entity, String.class);
		} catch (HttpServerErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
			assertThat(e.getResponseBodyAsString(), equalTo("Cannot extract features. Has " + MZN_FILENAME_NON_EXISITNG + " been uploaded?"));
		}
	}*/

	/*@Test
	public void testDeleteResult() {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(RESULT_ENDPOINT + "?userName=" + USER_1+"sunnyId=" + TEST_PID, HttpMethod.DELETE, entity, String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}*/


	private void triggerAssertions(ResponseEntity<String> response) {
		assertThat(response.getStatusCode(), equalTo(HttpStatus.ACCEPTED));
		assertThat(response.getBody(), equalTo("Server triggered the execution of sunny-cp process. Id in the headers"));
		assertTrue(response.getHeaders().containsKey("Sunny-Id"));
	}

	private void registerUser(String name, FileSystemResource mzn, FileSystemResource m) {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(mznFile, mzn);
		parts.add(model, m);
		parts.add(userName, name);
		register(parts);
	}

	private void registerUser(String name, FileSystemResource file) {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(mznFile, file);
		parts.add(userName, name);
		register(parts);
	}

	private void register(MultiValueMap parts) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity entity = createAuthorizationHeader(parts);
		ResponseEntity<String> response = restTemplate.postForEntity(FILES_ENDPOINT, entity, String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}

	private HttpEntity<String> createAuthorizationHeader(MultiValueMap<String, Object> parts) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(authorization, TEST_SID);
		return (HttpEntity<String>) new HttpEntity(parts, headers);
	}
}