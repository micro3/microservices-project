package group3.rest.beans;

import group3.App;
import group3.PasswordAuthentication;
import group3.entity.User;
import group3.repository.UserRepository;
import group3.sunny.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class UserControllerTest {

	@Autowired
	ServletWebServerApplicationContext server;

	@MockBean
	UserRepository userRepository;

	PasswordAuthentication passwordAuthentication = new PasswordAuthentication();

	@LocalServerPort
	int port;
	private String USER_ADDRESS = "";
	private String LOGIN_ADDRESS = "";
	private String LOGOUT_ADDRESS = "";
	private static final String USER_NAME_1 = "MarlonBrando";
	private static final String USER_NAME_2 = "AlPacino";
	private static final String USER_NAME_NON_EXISITNG = "BrigitteBardot";
	private static final String PASSWORD = "password";
	private static String HASHED_PASSWORD = "";
	private static final String WRONG_PASSWORD = "wrong_password";
	private static final String USER_NAME = "userName";
	private static final String TOKEN = "6f5ac1f1-34a0-4c89-98f8-b75445007f08";
	private static final String TEST_SID = "random_sid";
	private final String authorization = "authorization";
	private static User USER_1;

	@Before
	public void setup() {
		USER_ADDRESS = "http://localhost:" + port + "/user";
		LOGIN_ADDRESS = "http://localhost:" + port + "/user/login";
		LOGOUT_ADDRESS = "http://localhost:" + port + "/user/logout";
		HASHED_PASSWORD = passwordAuthentication.hash(PASSWORD.toCharArray());
		USER_1 = new User(USER_NAME_1, HASHED_PASSWORD, Role.ADMIN);
		Mockito.when(userRepository.findUserByUsernameAndSid(USER_NAME_1, TEST_SID))
				.thenReturn(Optional.of(USER_1));
	}

	@Test
	public void createUserTest() {
		Mockito.when(userRepository.findFirstByUsername(USER_NAME_NON_EXISITNG))
				.thenReturn(Optional.empty());

		MultiValueMap<String, Object> parts = createParts(USER_NAME_NON_EXISITNG, PASSWORD);
		parts.add("role", "ADMIN");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(USER_ADDRESS, parts, String.class);

		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
	}

	@Test
	public void createUserWithTheSameNameTest() {
		Mockito.when(userRepository.findFirstByUsername(USER_NAME_1))
				.thenReturn(Optional.of(USER_1));

		MultiValueMap<String, Object> parts = createParts(USER_NAME_1, PASSWORD);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.postForEntity(USER_ADDRESS, parts, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.CONFLICT));
			assertThat(e.getResponseBodyAsString(), equalTo("User name already taken"));
		}
	}

	/*@Test
	public void loginUserTest() {
		Mockito.when(userRepository.findFirstByUsername(USER_NAME_1))
				.thenReturn(Optional.of(USER_1));
		assertNull(USER_1.getSid());

		MultiValueMap<String, Object> parts = createParts(USER_NAME_1, PASSWORD);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(LOGIN_ADDRESS, parts, String.class);

		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertNotNull(USER_1.getSid());
	}*/

	@Test
	public void loginNonExistingUserTest() {
		Mockito.when(userRepository.findUserByUsernameAndPassword(USER_NAME_NON_EXISITNG, PASSWORD))
				.thenReturn(Optional.empty());

		MultiValueMap<String, Object> parts = createParts(USER_NAME_NON_EXISITNG, PASSWORD);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.postForEntity(LOGIN_ADDRESS, parts, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
			assertThat(e.getResponseBodyAsString(), equalTo("Given user does not exist"));
		}
	}

	@Test
	public void loginUserWithWrongPasswordTest() {
		Mockito.when(userRepository.findFirstByUsername(USER_NAME_1))
				.thenReturn(Optional.of(USER_1));

		MultiValueMap<String, Object> parts = createParts(USER_NAME_1, WRONG_PASSWORD);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.postForEntity(LOGIN_ADDRESS, parts, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
		}
	}

	@Test
	public void logoutUserTest() {
		USER_1.setSid(TOKEN);
		Mockito.when(userRepository.findFirstByUsername(USER_NAME_1))
				.thenReturn(Optional.of(USER_1));
		assertNotNull(USER_1.getSid());

		MultiValueMap<String, Object> parts = createTokenParts(USER_NAME_1, TOKEN);
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(LOGOUT_ADDRESS, HttpMethod.POST, entity, String.class);

		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertNull(USER_1.getSid());
	}

	@Test
	public void logoutNotLoggedUserTest() {
		Mockito.when(userRepository.findUserByUsernameAndSid(USER_NAME_2, TOKEN))
				.thenReturn(Optional.empty());

		MultiValueMap<String, Object> parts = createTokenParts(USER_NAME_1, TOKEN);
		HttpEntity<String> entity = createAuthorizationHeader(parts);
		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(LOGOUT_ADDRESS, HttpMethod.POST, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
		}
	}

	@Test
	public void logoutNotExistingUserTest() {
		Mockito.when(userRepository.findUserByUsernameAndSid(USER_NAME_NON_EXISITNG, TOKEN))
				.thenReturn(Optional.empty());

		MultiValueMap<String, Object> parts = createTokenParts(USER_NAME_NON_EXISITNG, TOKEN);
		HttpEntity<String> entity = createAuthorizationHeader(parts);

		RestTemplate restTemplate = new RestTemplate();
		try {
			restTemplate.exchange(LOGOUT_ADDRESS, HttpMethod.POST, entity, String.class);
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
		}
	}

	private MultiValueMap<String, Object> createTokenParts(String userName1, String token) {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(USER_NAME, userName1);
		parts.add("token", token);
		return parts;
	}

	private MultiValueMap<String, Object> createParts(String userName, String password) {
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add(USER_NAME, userName);
		parts.add(PASSWORD, password);
		return parts;
	}

	private HttpEntity<String> createAuthorizationHeader(MultiValueMap<String, Object> parts) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(authorization, TEST_SID);
		return (HttpEntity<String>) new HttpEntity(parts, headers);
	}
}
