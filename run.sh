#!/bin/bash
ZERO=0

python /tool/sunny-cp/src/sunny_server.py &

if [[ "$PACKAGE" -eq "$ZERO" ]]; then
    java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 $LOC/target/sunny_cp_project-${VER}.jar
else # Package based deploymeny
    mvn dependency:get \
    -DremoteRepositori=gitlab.com/api/v4/projects/9265749/packages/maven \
    -DgroupId=microservices_group_3 \
    -DartifactId=sunny_cp_project \
    -Dtransitive=false \
    -Dversion=$VER
    java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 /root/.m2/repository/microservices_group_3/sunny_cp_project/${VER}/sunny_cp_project-${VER}.jar
fi
