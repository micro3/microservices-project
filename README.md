# Microservices Project

DM848 final project by Group 3. The aim of this project is to bring a legacy application sunny-cp into the cloud.

[![License: GPL v3](https://img.shields.io/badge/dynamic/json.svg?url=http://gitlab.phillwide.cz/version/&query=$.*&label=version)](VERSION)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](LICENCE)
[![pipeline status](https://gitlab.com/micro3/microservices-project/badges/master/build.svg)](https://gitlab.com/micro3/microservices-project/commits/master)

## Sunny-cp

Sunny-cp is a parallel portfolio solver that allows solving a Constraint Problem (CP) defined in the MiniZinc language. It was developed in python and can be deployed using a rudimentary Dockerfile.

In general, a user can use sunny-cp for performing the following tasks:

- solving CPs encoded in Minizinc using a combination of solvers
- extracting from a CP a feature vector capturing with numbers the "nature of the problem"
- adding and creating a knowledgebase used to solve faster similar CPs in the future
- adding new constraint CP solver.

See [Sunny-cp git](https://github.com/CP-Unibo/sunny-cp)

## MiniZinc

MiniZinc is a free and open-source constraint modeling language supported by many CP solvers.

See [MiniZinc](https://www.minizinc.org/)

## Setup

For setup read documentation located in doc folder.
