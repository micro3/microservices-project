\section{Database description}\label{dbd}

We chose to use the Heroku service for our database, since they provide an easy to use cloud database solution, where we don't need to worry about anything other than what we want in the database, and can just let their system take care of stuff such as sleeping when not in use.

While Heroku does provide a really easy way to access the database no matter where it might have moved to, using an environment variable called DATABASE\_URL that is set by Heroku when run, this requires having Heroku installed, running, and most importantly, logged in to an account, on all of our docker images. And while it is possible to do so, Heroku doesn't really have a good way to login through a script, without human intervention. Therefore we ended up just using the DATABASE\_URL to get the address and credentials of the database once, and then access the database using those directly. This does mean that if the database is moved for some reason (something Heroku warns can happen, although rarely), we would have to manually go in and find the new address.

The way to do it properly would be to have Heroku installed on the docker images, which can easily be done, and then have the API key and username of a Heroku Account with access to the Heroku project that has the database encoded as a protected variable in Gitlab. Then we can have those variables saved in the file that Heroku checks for credentials in, meaning that we will look like we've logged into the account, and we will then be able to use the DATABASE\_URL variable, which Heroku will keep up to date.

However, since this requires including the API key of an account in the code of the project, even in protected mode, and would have required some changes to the code, we decided we didn't want to try and implement it last minute.

\subsection{Setting up a new Database with Heroku}
The way we set up this database in Heroku, and therefore also the way you would set up a new one if you wanted to, is to first have heroku installed and logged in as explained in the Preliminaries.
Then, run \textbf{heroku create (app name)} in a folder to create a Heroku app with the requested name, and \textbf{heroku addons:create heroku-postgresql:hobby-dev -a (app name)}. This will add a PSQL database with the plan Hobby Dev, which is the free one, to your heroku app.

Then, use \textbf{heroku pq:psql -a (app name)} to get a direct psql console to the database, and copy in the create scripts from the file, and you should have a working new database.


\section{Database tables} \label{db}
We decided that we needed 3 different tables, one for the users and all of their data, one for the various files, and one for the currently active CP processes.

\subsection{users}
This table is for storing the usernames of our users, as well as their passwords, their current session ID, and their role.
This tables usernames column is also used as a foreign key for the 2 other tables username columns, since if the user doesn't exist, they shouldn't be able to store files or start processes.

\subsubsection{id}
Datatype Integer

This is just for easy indexing and viewability, and isn't actually used that much, since unlike the 2 other tables this tables primary key is the usernames column.
\subsubsection{usernames}
Datatype Text

This is for storing the username of the user. Since we don't want users with the same username as each other, this is the primary key for this table, meaning that all entries must be unique.
\subsubsection{password}
Datatype Text

For storing the passwords of the users, so we can compare the one they entered when trying to log in. Stored password in the database is hashed. When a user wants to log in, they provide the password, and the given password is then compared with the hash of the password stored in the database. If the comparison is successful, user is granted an access to their account.
\subsubsection{sid(Session ID)}
Datatype Text

For storing the last used Session ID of the user, so they don’t have to constantly log in. Once they log in this value gets set, and is then just kept until they login again, at which point it gets overwritten. every time that the user wants to interact with our system, they have to provide the current (SID). This does mean that a user can only be "logged in" on one device at a time, but i think that's acceptable.

\subsubsection{role}
Daatype Integer

For storing if the user is a standard user (0), or an admin (1) which has access to some advanced options such as deleting files of other users. This is stored as an int for simplicity, and that int is then interpreted in the other parts.

\subsection{files}
For storing the files of the users, so they can run the solvers with them.
Files with the results are also stored in this table, so users can access them later on.

\subsubsection{id}
Datatype Integer

Again, just to have a number available. Here it's also the primary key of the table.
\subsubsection{usernames}
Datatype Text

For storing the username that owns the file in this row. Foreign keyed off of the usernames column in the users table, so only usernames that occur there can be used. This is needed because different users are allowed to upload similarly named files, so we need something other than filename to look after, and because users or admins need to be able to see all of the files they've uploaded, and this is the easiest way to do that.

\subsubsection{filename}
Datatype Text

For storing the filename of the file stored here. We could probably get that info directly from the file, but we figured it was easier to simply save it here, and not have to worry about extracting it each time. Also, the same problem with cloud services as described in the "file" section applies here.

\subsubsection{file}
Datatype BYTEA

For storing the actual file. Apparently there's 2 common ways of storing files in a psql database: Using the bytea type, or simply saving the file on the drive of the db and then saving the path as a text. But the latter won't work for us since our db is in the cloud and might not be on the same physical device all the time, so we're using bytea.

\subsubsection{filetype}
Datatype Integer

To make it easier to get a list of all of one type of files, probably mostly results, the type of the file is stored here as an Integer, so 1 for mzn, 2 for dzn, 3 for result and so on.

\subsection{processes}
For storing the currently running processes of all of the users, both so that the frontend can see if a computation is done, but also so that admins can find and stop processes of users.
Once a process is done, the row isn't removed until the frontend ask for it, at which point the result is sent to the frontend AND stored in the Files table for later access, and the row is deleted.

\subsubsection{id}
Datatype Integer

Again, just to have a number available. Here it's also the primary key of the table.

\subsubsection{usernames}
Datatype Text

For storing the username that started the process, both for info to the admins, but also so we know who to attribute the result to once done.
Foreign keyed off of the usernames column in the users table, so only usernames that occur there can be used.

\subsubsection{pid (Process ID)}
Datatype Text

For storing the Process ID of the process, so that we can update the state, and the frontend can ask for that state.

\subsubsection{state}
Datatype Integer

For storing the current state of the process as an int, that will then be interpreted by the frontend to inform the user. An example would be 1 = running, 2 = crashed, 3 = error, and 4 = result.

\section{Related Work and Discussion}
\subsection{Database}
We decided pretty quickly on Heroku, mainly because it was easy and simple to use, but also because it was relatively late in the project that we figured out we would need a database, and Heroku was one of the first free solutions we came upon.
In general, there isn't a lot of free Database as a Service (DaaS) solutions out there, but some of the other ones are solutions such as Amazon RDS, which has 12 months free trial which would be plenty for our project, or MongoDB Atlas, but none of them seem to have quite as thorough a documentation or API as Heroku does, or even less free storage. (Heroku has no cap on uptime, but free DBs will sleep after a certain time not in use, and can only hold 10k rows of data, none of which are a problem for us).

We could also have chosen to do the entire thing ourselves, but since this project wasn't really mainly about setting up a database on a cloud service, i don't think that would be a good use of our time or resources.

Only tiny problem with Heroku was the programmatic access to the database, as described in the Database Explanation section.
