
\section{API description} \label{apidesc}
In the system there are two types of users - standard \textit{user} and \textit{admin}. At the beginning a user has to create an account. All the files that the user uploaded will be associated only with their account. The user will have an access only to their files and other users are not going to be able to access the files of the first user. In case the user has already created the account, they have to log into the system with the same credentials given during the process of account creation.

 A user has a possibility to upload all the files that are necessary for them to solve the constraints ( \textit{.mzn},  \textit{.dzn},  \textit{.model.mzn} etc.). The user has a possibility to remove all the files that have been already uploaded. Also all the results obtained by the user are possible to be removed.  After that the user can trigger the execution of Sunny-CP program with specified file and parameters (solvers, backup solvers for portfolio, timeout etc.). Since the execution can take a lot of time, the result of the execution is not given immediately after the trigger. As a result of initiating the execution, a user gets an id of the execution that belongs to them. This id will be used to obtain the result of the execution upon its termination. If the user tries to get the result before that, they will be informed that the execution has not ended yet. Otherwise, the result will be given to the user. 
 
 All the features mentioned in previous paragraph are available for both standard users and admins.
 If the user has a role of \textit{admin}, they have some additional possibilities. For instance, the admin can remove all the files that belong to a user. Furthermore admin can also remove the user itself from the system.

\subsection{Login}  \label{loginlabel}
In order to keep a user logged in, every request sent to the server must contain \textit{authorization} header. This header contains token (session ID) that defines if the user's session is still valid. From this point I will assume that every request contains previously mentioned \textit{authorization} header, unless otherwise specified. Because of this, almost every request will have possible the following response:
\begin{itemize}
  \item 401 (unauthorized) - session id sent in the request is not recognizable by the server. Request may not contain an \textit{authorization} header or session id is not correct.
\end{itemize}

\subsubsection{Endpoint \textit{/user}, POST}
This endpoint is used to create a user. From obvious reasons this request does not demand \textit{authorization} header.

Body parameters:
\begin{itemize}
  \item userName - string that contains name of the user.
  \item password - string that contains password of the user.
  \item role - string that defines the role of a user. So far we have two roles - \textit{USER} and \textit{ADMIN}.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 409 (conflict) - user name has been already taken.
  \item 201 (created) - user has been successfully created
\end{itemize}




\subsubsection{Endpoint \textit{/user/login}, POST}
This endpoint is used to login a user. It is not necessary to send \textit{authorization} header.

Body parameters:
\begin{itemize}
  \item userName - string that contains name of the user.
  \item password - string that contains password of the user.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (not found) - provided user name or password is not correct
  \item 200 (OK) - user has been successfully logged in. As a response body frontend receives Json with user name and current \textbf{sid} (session id). This \textbf{sid} will be used later on to keep the session alive. It is a parameter that will be sent in \textit{authorization} header.
\end{itemize}




\subsubsection{Endpoint \textit{/user/logout}, POST}
This endpoint is used to log out a user out of the system.

Body parameters:
\begin{itemize}
  \item userName - string that contains name of the user.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (Not found) - user is not logged into system. Most probably their \textbf{sid} is not in a DB.
  \item 200 (OK) - user has been successfully logged out.
\end{itemize}




\subsection{Sunny endpoints}
\subsubsection{Endpoint \textit{/files}, POST}
Used to upload all the files that will be used to trigger sunny-cp execution.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name.
\end{itemize}


Request parameters:
\begin{itemize}
  \item mznFile - (necessary) file with extension .mzn that defines problem to be solved. (ex. tenpenki\_1.mzn)
  \item dznFile - (optional)  file with extension .dzn that defines data to be used when solving problem.
  \item model - (optional) file with defined model (ex. tenpenki.model.mzn)
\end{itemize}

Possible responses:
\begin{itemize}
  \item 400 (Bad request) - when either userName or mznFile are not provided (or both).
  \item 500 (Internal server error) - when received files cannot be saved to disk.
  \item 200 (OK) - on successfully saving the files.
\end{itemize}




\subsubsection{Endpoint \textit{/trigger}, GET}
Used to trigger the execution of sunny-cp with specified parameters and files.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name.
  \item mznFileName - (necessary) Name of the file  that is to be triggered and was uploaded in previous step
\end{itemize}


Request parameters:
\begin{itemize}
  \item modelFileName -  (optional) name of the model file that was uploaded in files post endpoint and is to be used in this execution of sunny-cp.
  \item dznFileName - (optional) name of the .dzn file that was uploaded in files post endpoint and is to be used in this execution of sunny-cp.
  \item -P  - (optional) - name of the solver that is to be used in the execution. Currently at least one of the following:
  \textit{yuck, haifacsp, minisatid, jacop, picat, choco, ortools, chuffed, g12osicbc, gecode}.
  \item -T - (optional) timeout - integer.
  \item -K - (optional) knowledge base - not tested yet.
  \item -R - (optional) name of the solver (or solvers) that are to be excluded from current sunny-cp execution.
  \item -A - (optional) name of the solver (or solvers) that are to be added to current pool of solvers in sunny-cp execution.
  \item -b - (optional) backup solver for portfolio.
  \item -s - (optional) schedule for selected solver (max time that solver will be trying to solve a problem).
  \item -p - (optional) number of cores used in execution. By default all the cores available to the server are used.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 204 (Accepted) -  command to execute sunny-cp has been received and server started processing execution.
Response body contains a message (“Server triggered the execution of sunny-cp process. Id in the headers”)
Custom headers:
Sunny-Id - string that identifies the execution. It will be used in endpoint \ref{result}.
\end{itemize}


\subsubsection{Endpoint \textit{/result}, GET}  \label{result}
Used to get a result of the sunny-cp execution triggered in previous endpoint.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name.
  \item sunnyId -  (necessary) id of the execution received as a header in previous endpoint.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (Not found) - when sunnyId given as a parameter does not exist.
  \item 200 (OK) - when execution has been triggered and exists in a system. This response contains Json with state of the execution (\textit{success, in\_progress, error}). In case of \textit{success}, json contains also the result of the execution.
\end{itemize}






\subsubsection{Endpoint \textit{/features}, GET}
Used to get feature vector of previously uploaded files.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name.
  \item mznFileName - (necessary) Name of the file that will be used to extract features.
\end{itemize}

Request parameters:
\begin{itemize}
  \item dznFileName - (optional) name of the .dzn file that was uploaded in files post endpoint and is to be combined with .mzn file to extract features from both files.
  \item modelFileName - (optional) name of the model.mzn file, that is to be used when extracting features from .mzn file.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (Not found) - when system does not contain .mzn file with name specified in path parameters.
  \item 500 (Internal server error) - when sunny-cp cannot extract features from .mzn file. In this case it is probably due to lack of some necessary files (like .dzn or model).
  \item 200 (OK) - on successfully extracting the features. Body of the response contains text received from sunny-cp.
\end{itemize}



\subsubsection{Endpoint \textit{/files}, DELETE}
Used to remove uploaded files from system.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name.
  \item fileName - (necessary) Name of the file that will be removed.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (Not found) -  when file intended to be deleted does not exist.
  \item 200 (OK) - when file has been deleted successfully.
\end{itemize}








\subsection{Admin}
\subsubsection{Endpoint \textit{/admin/files}, DELETE}
Allows an admin to remove all the files that were uploaded by specified user.

Path parameters:
\begin{itemize}
  \item userName - (necessary) user name. This user must have role \textit{ADMIN}, in order to be able to remove the files.
\end{itemize}

Request parameters:
\begin{itemize}
  \item filesUserName - A name of the user whose files will be deleted.
\end{itemize}

Possible responses:
\begin{itemize}
  \item 404 (Not found) -  when user does not own any files in the system.
  \item 200 (OK) - when all files have been deleted successfully.
\end{itemize}
